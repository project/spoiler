# Spoiler

This module allows users to hide potential "spoiler" content by inserting them
between [spoiler][/spoiler] tags. These tags will be converted to HTML by this
filter and the relevant CSS rules will set the foreground and background colours
to the same value, thereby rendering the text invisible until highlighted.

If Javascript is available, the spoiler is replaced with a clickable (CSS)
button, which when clicked reveals the content.

The module provides a theme function named theme_spoiler which can be
overridden to make changes to the markup etc. 

For a full description of the module, visit the
[project page](https://www.drupal.org/project/spoiler).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/spoiler).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to Administration » Configuration » Content authoring » Text formats
   and editors.
2. Edit a text format, for example "Basic HTML".
3. Enable a "Spoiler filter" filter and configure it under "Filter settings".


## Maintainers

- [Damien McKenna](https://www.drupal.org/u/damienmckenna)
- [Karthik "zen" Kumar](https://www.drupal.org/u/zen)
- [Morbus Iff](https://www.drupal.org/u/morbus-iff)

<?php

namespace Drupal\spoiler\Plugin\Filter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Renderer;
use Drupal\filter\Annotation\Filter;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Replaces the [spoiler] shortcode with some custom CSS/JS.
 *
 * @Filter(
 *   id = "spoiler",
 *   title = @Translation("Spoiler filter"),
 *   description = @Translation("Converts [spoiler][/spoiler] and [spoilers][/spoilers] shortcodes to markup which hides the content within. When Javascript is available, a button is made available which can be clicked to view the hidden content. Alternatively, when Javascript is disabled, the filter sets the foreground and background colours to the same value, effectively rendering the content within invisible until highlighted."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   weight = 100
 * )
 */
class Spoiler extends FilterBase implements ContainerInjectionInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = \Drupal::service('renderer');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    // Keep it simple. Use of the lazy quantifier allows use of multiple spoiler
    // blocks but does not address nested spoilers.
    $result->setProcessedText(preg_replace_callback(
      '#\[\s*spoiler\s*\](.*?)\[\s*/\s*spoiler\s*\]#is',
      function ($matches) {
        $output = [
          '#theme' => 'spoiler',
          '#spoiler' => $matches[1],
        ];
        return (string) $this->renderer->renderPlain($output);
      },
      $text
    ));
    $result->addAttachments([
      'library' => [
        'spoiler/spoiler',
      ],
    ]);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Potential spoilers can be hidden between [spoiler][/spoiler] tags to hide them by default.');
  }

}
